With docker running:
`compser install`
Ensure correct settings in app/config/database.php
Migrate the database: `php artisan migrate`
Seed the database: `php artisan db:seed`