<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'name'     => 'Andrew Curlett',
			'username' => 'B00757516',
			'email'    => 'curlett-a@ulster.ac.uk',
			'password' => Hash::make('examplepass'),
		));
	}

}
?>