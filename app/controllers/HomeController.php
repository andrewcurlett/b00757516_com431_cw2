<?php

class HomeController extends BaseController {

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function showLogin()
	{
		// show the form
		return View::make('login');
	}

	public function doLogin()
	{
		// rules to valdate credentials
		$rules = array(
			'email'    => 'required|email', // ensures input is an email address
			'password' => 'required|alphaNum|min:3' // ensures password is within parameters
		);

		// validates credentials using given rules 
		$validator = Validator::make(Input::all(), $rules);

		// if validation fails redirect to form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // sends errors to login form
				->withInput(Input::except('password')); // returns given credentials minus the password
		} else {

			// create user data for authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);

			// attempts login
			if (Auth::attempt($userdata)) {

				// successful validation redirects to welcome page
				return Redirect::to('hello');

			} else {

				// unsuccessful validation returns to form
				return Redirect::to('login');

			}

		}
	}

	public function doLogout()
	{
		Auth::logout(); // logs out the user
		return Redirect::to('login'); // redirects to login page
	}

}
?>